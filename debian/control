Source: extension-helpers
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: cython3 <!nocheck>,
               debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-pip,
               python3-pytest,
               python3-setuptools,
               python3-setuptools-scm,
               python3-tomli
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/extension-helpers
Vcs-Git: https://salsa.debian.org/python-team/packages/extension-helpers.git
Homepage: https://pypi.org/project/extension-helpers/

Package: python3-extension-helpers
Architecture: any
Depends: python3-setuptools,
         python3-dev,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Description: Utilities for building and installing packages
 The extension-helpers package includes convenience helpers to assist
 with building Python packages with compiled C/Cython extensions. It is
 developed by the Astropy project but is intended to be general and
 usable by any Python package.
 .
 This is not a traditional package in the sense that it is not intended
 to be installed directly by users or developers. Instead, it is meant to
 be accessed when the setup.py command is run and should be defined as a
 build-time dependency in pyproject.toml files.
