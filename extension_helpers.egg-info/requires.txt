setuptools>=40.2

[:python_version < "3.11"]
tomli>=1.0.0

[docs]
sphinx
sphinx-automodapi

[test]
wheel
pytest
pytest-cov
cython
